Simple reverse shell in Python written with the goal of taking over the
machines of colleagues who forget to lock them when leaving their desks.

Specify the _CONNECT_URI_ which should have a file with the IP:PORT that
the backdoor will try to connect to. In case of failure or after disconnecting
from the shell you will have KEEP_ALIVE seconds to resume the connection,
otherwise the backdoor will quit.

To run the listening server execute `nc -l IP PORT -vvv`
